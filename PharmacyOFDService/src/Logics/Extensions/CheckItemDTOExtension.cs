﻿using System.Collections.Generic;
using TS2OFDService.Models;

namespace TS2OFDService.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    internal static class CheckItemDTOExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="checkItemDtos"></param>
        /// <returns></returns>
        internal static List<DocumentCheckItem> ToDocumentCheckItem(this List<DocumentCheckItemDTO> checkItemDtos)
        {
            if (checkItemDtos == null)
                return new List<DocumentCheckItem>();

            var checkitems = new List<DocumentCheckItem>();
            foreach (var itemDto in checkItemDtos)
            {
                checkitems.Add(new DocumentCheckItem
                {
                    Amount = (int)(itemDto.Amount * 1000),
                    Discount = (int)(itemDto.Discount * 100),
                    Price = (int)(itemDto.Price * 100),
                    VAT = (int)(itemDto.VAT * 100),
                    Other = (int)(itemDto.Other * 100),
                    Barcode = itemDto.Barcode,
                    Name = itemDto.Name
                });
            }

            return checkitems;
        }
    }
}