﻿using TS2OFDService.Models;

namespace TS2OFDService.Extensions
{
    internal static class ReceiptDTOExtension
    {
        internal static Receipt ToReceipt(this ReceiptDTO receiptDto)
        {
            var receipt = new Receipt
            {
                Time = receiptDto.Time.ToFormattableString(),
                ReceivedCash = decimal.ToInt32(receiptDto.ReceivedCash * 100),
                ReceivedCard = decimal.ToInt32(receiptDto.ReceivedCard * 100),
                Items = receiptDto.Items.ToDocumentCheckItem()
            };
            return receipt;
        }
    }
}