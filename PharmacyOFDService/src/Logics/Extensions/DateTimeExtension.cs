﻿using System;

namespace TS2OFDService.Extensions
{
    internal static  class DateTimeExtension
    {
        internal static string ToFormattableString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}