﻿using System;

namespace TS2OFDService.Models
{
    /// <summary>
    /// Результат пост запроса
    /// </summary>
    public class ResponseResult : IResponseResult
    {
        public string TerminalID { get; set; }
        public int ReceiptSeq { get; set; }
        public DateTime DateTime { get; set; }
        public string FiscalSign { get; set; }
        public int AppletVersion { get; set; }
        public string QRCodeURL { get; set; }
    }
}