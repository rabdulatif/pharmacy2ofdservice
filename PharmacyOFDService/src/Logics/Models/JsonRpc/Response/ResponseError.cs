﻿namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ResponseError
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public Data Data { get; set; }
    }
}