﻿namespace TS2OFDService.Models
{
    public class BaseResponseModel
    {
        private string JsonRpc { get; set; }
        public ResponseResult Result { get; set; }
        public ResponseError Error { get; set; }
        public int Id { get; set; }

        public string Content { get; set; }
    }
}