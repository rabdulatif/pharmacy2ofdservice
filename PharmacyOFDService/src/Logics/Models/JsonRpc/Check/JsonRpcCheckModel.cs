﻿namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonRpcCheckModel
    {
        public string Method { get; set; }
        public int  Id { get; set; }
        public JsonRpcParams Params { get; set; }
        public string Jsonrpc { get; set; }
    }
}