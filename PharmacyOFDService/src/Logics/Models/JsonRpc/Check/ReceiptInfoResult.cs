﻿using Newtonsoft.Json;
using System;

namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ReceiptInfoResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string TerminalID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ReceiptSeq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? TransactionTimeDateTime
        {
            get
            {
                if (string.IsNullOrEmpty(TransactionTimeString))
                    return null;
                return DateTime.Parse(TransactionTimeString);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "TransactionTime")]
        public string TransactionTimeString { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TotalVAT { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TotalCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TotalCash { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Sale { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppletVersion { get; set; }
    }
}