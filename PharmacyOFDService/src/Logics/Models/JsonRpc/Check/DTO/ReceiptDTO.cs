﻿using System;
using System.Collections.Generic;

namespace TS2OFDService.Models
{
    /// <summary>
    /// Чек DTO
    /// </summary>
    public class ReceiptDTO
    {
        /// <summary>
        /// Время
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Позиции чека
        /// </summary>
        public List<DocumentCheckItemDTO> Items { get; set; }

        /// <summary>
        /// Полученная от покупателя сумма наличными
        /// </summary>
        public decimal ReceivedCash { get; set; }

        /// <summary>
        /// Полученная от покупателя сумма посредством пластиковой карты
        /// </summary>
        public decimal ReceivedCard { get; set; }
    }
}