﻿namespace TS2OFDService.Models
{
    /// <summary>
    /// Позиции чека(ДТО модель для пользователя)
    /// </summary>
    public class DocumentCheckItemDTO
    {
        /// <summary>
        /// Общая сумма позиции без учета скидок
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Товарный код
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// Количество товара
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// НДС сумма
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Наименование товара или услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cкидка
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Прочие (Оплата по страховки и др.)
        /// </summary>
        public decimal Other { get; set; }
    }
}