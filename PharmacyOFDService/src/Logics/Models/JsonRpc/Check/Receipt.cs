﻿using System;
using System.Collections.Generic;

namespace TS2OFDService.Models
{
    public class Receipt
    {
        /// <summary>
        /// Время
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DocumentCheckItem> Items { get; set; }

        /// <summary>
        /// Полученная от покупателя сумма наличными
        /// </summary>
        public int ReceivedCash { get; set; }

        /// <summary>
        /// Полученная от покупателя сумма посредством пластиковой карты
        /// </summary>
        public int ReceivedCard { get; set; }
    }
}