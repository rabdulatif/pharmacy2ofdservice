﻿
namespace TS2OFDService.Models
{
    public class UnsendCheckResponseModel
    {

        private string JsonRpc { get; set; }
        public ReceiptInfoResult Result { get; set; }
        public ResponseError Error { get; set; }
        public int Id { get; set; }

        public string Content { get; set; }
    }
}