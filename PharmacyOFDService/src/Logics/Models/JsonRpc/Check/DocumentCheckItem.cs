﻿
namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentCheckItem
    {
        /// <summary>
        /// Общая сумма позиции без учета скидок
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Товарный код
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// Количество товара
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// НДС сумма
        /// </summary>
        public int VAT { get; set; }

        /// <summary>
        /// Наименование товара или услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cкидка
        /// </summary>
        public int Discount { get; set; }

        /// <summary>
        /// Прочие (Оплата по страховки и др.)
        /// </summary>
        public int Other { get; set; }
    }
}