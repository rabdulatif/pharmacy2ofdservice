﻿
namespace TS2OFDService.Models
{
    public class FiscalMachineResponseModel
    {
        private string JsonRpc { get; set; }
        public FiscalMachineResponseResult Result { get; set; }
        public ResponseError Error { get; set; }
        public int Id { get; set; }

        public string Content { get; set; }
    }
}