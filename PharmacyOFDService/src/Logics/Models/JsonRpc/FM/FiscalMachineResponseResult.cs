﻿namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FiscalMachineResponseResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string AppletVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ReceiptCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ReceiptMaxCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ZReportCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ZReportMaxCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AvailablePersistentMemory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AvailableResetMemory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AvailableDeselectMemory { get; set; }
    }
}