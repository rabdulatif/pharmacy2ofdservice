﻿namespace TS2OFDService.Models
{
    public class Data
    {
        public string JsonRpc => "2.0";
        public string Method { get; set; }
        public JsonRpcParams Params { get; set; }
        public int Id { get; set; }
    }
}