﻿namespace TS2OFDService.Models
{
    public class JsonRpcShiftModel
    {
        public string Method { get; set; }
        public int Id { get; set; }
        public JsonRpcShiftParams Params { get; set; }
        public string Jsonrpc { get; set; }
    }
}