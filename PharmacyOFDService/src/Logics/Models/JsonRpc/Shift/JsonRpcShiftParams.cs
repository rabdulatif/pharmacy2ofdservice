﻿namespace TS2OFDService.Models
{
    public class JsonRpcShiftParams
    {
        /// <summary>
        /// 
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Receipt Receipt { get; set; }
    }
}