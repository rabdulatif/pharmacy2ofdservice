﻿using Newtonsoft.Json;
using System;

namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ShiftResponseResult : IResponseResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string TerminalID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CloseTimeDateTime
        {
            get
            {
                if (string.IsNullOrEmpty(CloseTimeString))
                    return null;
                return DateTime.Parse(CloseTimeString);
            }
        }

        [JsonProperty(PropertyName = "CloseTime")]
        public string CloseTimeString { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LastReceiptSeq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FirstReceiptSeq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? OpenTimeDateTime
        {
            get
            {
                if (string.IsNullOrEmpty(OpenTimeString))
                    return null;
                return DateTime.Parse(OpenTimeString);
            }
        }
        [JsonProperty(PropertyName = "OpenTime")]
        public string OpenTimeString { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalRefundVAT { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalRefundCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalRefundCash { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalRefundCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalSaleVAT { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalSaleCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalSaleCash { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalSaleCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AppletVersion { get; set; }
    }
}
