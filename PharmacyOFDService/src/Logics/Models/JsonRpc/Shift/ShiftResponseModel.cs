﻿namespace TS2OFDService.Models
{
    public class ShiftResponseModel
    {
        private string JsonRpc { get; set; }
        public ShiftResponseResult Result { get; set; }
        public ResponseError Error { get; set; }
        public int Id { get; set; }

        public string Content { get; set; }
    }
}