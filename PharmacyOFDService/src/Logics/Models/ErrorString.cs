﻿namespace TS2OFDService.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ErrorString
    {
        public const string ERROR_RECEIPT_COUNT_ZERO = "Количество чеков равно нулю";
        public const string ERROR_RECEIPT_INDEX_OUT_OF_BOUNDS = "Номер чека не правильный";
        public const string ERROR_RECEIPT_NOT_FOUND = "Чек не найден";
        public const string ERROR_DATA_SIZE_NOT_SUPPORTED = "Размер информации не поддерживается";
        public const string ERROR_RECEIPT_FORMAT_INVALID = "Формат чека на правильный";
        public const string ERROR_RECEIPT_TOTAL_PRICE_OVERFLOW = "Общая сумма превышает максимального значение";
        public const string ERROR_RECEIPT_TOTAL_PRICE_MISMATCH = "Общая сумма превышает стоимость по товарным позициям";
        public const string ERROR_RECEIPT_MEMORY_FULL = "Память чека заполнена";
        public const string ERROR_RECEIPT_TIME_PAST = "Время чека старое";
        public const string ERROR_RECEIPT_STORE_DAYS_LIMIT_EXCEEDED = "Кол-во дней хранения чеков превышено, следует отправить чеки";
        public const string ERROR_LAST_TRANSACTION_TIME_FORMAT_INVALID = "Формат времени последней транзакции ошибочна";
        public const string ERROR_FIRST_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID = "Формат времени чека ошибочная";
        public const string ERROR_ACKNOWLEGE_WRONG_LENGTH = "Ошибка сервера ОФД по длине строк";
        public const string ERROR_ACKNOWLEGE_SIGNATURE_INVALID = "Ошибка сервера ОФД по подписи чека";
        public const string ERROR_ACKNOWLEGE_TERMINAL_ID_MISMATCH = "Ошибка сервера ОФД по номеру ФМ (все три позиции связаны с несанкционированным доступом к серверу ОФД)";
        public const string ERROR_ZREPORT_PARTITION_INVALID = "--";
        public const string ERROR_OPEN_CLOSE_ZREPORT_WRONG_LENGTH = "Ошибка связана с длиной строки Z-report";
        public const string ERROR_CLOSE_ZREPORT_TIME_PAST = "Время закрытие чека старое";
        public const string ERROR_CURRENT_TIME_FORMAT_INVALID = "Формат текущего времени ошибочна";
        public const string ERROR_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID = "Формат времени последнего отправленного чека ошибочна";
        public const string ERROR_ZREPORT_INDEX_OUT_OF_BOUNDS = "Номер Z-report не правильный";
        public const string ERROR_LOCK_CHALLENGE_INVALID = "--";
        public const string ERROR_LOCKED_FOREVER = "Фискальный модуль заблокирован";
        public const string ERROR_ZREPORT_SPACE_IS_FULL = "Память Z-report заполнена";
        public const string ERROR_CURRENT_ZREPORT_IS_EMPTY = "Текущий Z-report пустой";
        public const string ERROR_RECEIPT_TOTAL_PRICE_ZERO = "Общая сумма чека не может быть нулем";
        public const string ERROR_ZREPORT_IS_NOT_OPEN = "Z-report не открыт";
        public const string ERROR_ZREPORT_OPEN_TIME_FORMAT_INVALID = "Формат времени открытия Z-report ошибочна";
        public const string ERROR_SALE_REFUND_COUNT_OVERFLOW = "Превышено кол-во операций (продажи и возврата) в Z-отчете";
        public const string ERROR_ZREPORT_IS_ALREADY_OPEN = "Z-report уже открыт";
        public const string ERROR_NOT_ENOUGH_CASH_FOR_REFUND = "Не достаточно средств для возврата (наличка)";
        public const string ERROR_NOT_ENOUGH_CARD_FOR_REFUND = "Не достаточно средств для возврата (пластик)";
        public const string ERROR_NOT_ENOUGH_VAT_FOR_REFUND = "Не достаточно средств для возврата (НДС)";
        public const string ERROR_OPEN_ZREPORT_TIME_PAST = "Время открытия Z-report старое";
        public const string ERROR_MAINTENANCE_REQUIRED = "Требуется обслуживание со стороны ОФД";


    }
}
