﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using TS2OFDService.Helpers;
using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class FiscalMachineService : IFiscalMachineService
    {
        /// <summary>
        /// 
        /// </summary>
        private HttpClient client;
        private JsonSerializerSettings settings;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rpcApiAddress"></param>
        public FiscalMachineService(string rpcApiAddress)
        {
            if (client == null)
                client = new HttpClient { BaseAddress = new Uri(rpcApiAddress) };
            settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public FiscalMachineResponseModel GetFMStats()
        {
            var jsonRpc = MakeRPCStatsModel();
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<FiscalMachineResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        private JsonRpcShiftModel MakeRPCStatsModel()
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.GetInfo",
                Id = 40000,
                Params = new JsonRpcShiftParams(),
                Jsonrpc = "2.0"
            };
        }

        public bool IsFmAviable()
        {
            var fiscalMachine = GetFMStats();
            if (IsShiftAviable(fiscalMachine) && IsReceiptAviable(fiscalMachine) && IsMemoryAviable(fiscalMachine))
                return true;
            return false;
        }

        private bool IsShiftAviable(FiscalMachineResponseModel fiscalMachine)
        {
            if (fiscalMachine.Result?.ZReportCount >= fiscalMachine.Result?.ZReportMaxCount)
                throw new Exception("Максимальное количество смен заполнено. Пожалуйста, свяжитесь с технической службой");
            return true;
        }
        private bool IsReceiptAviable(FiscalMachineResponseModel fiscalMachine)
        {
            if (fiscalMachine.Result?.ReceiptCount >= fiscalMachine.Result?.ReceiptMaxCount)
                throw new Exception("Максимальное количество чеков заполнено. Пожалуйста, свяжитесь с технической службой");
            return true;
        }
        private bool IsMemoryAviable(FiscalMachineResponseModel fiscalMachine)
        {
            if (fiscalMachine.Result?.ReceiptCount >= fiscalMachine.Result?.ReceiptMaxCount)
                throw new Exception("Память закончилась. Пожалуйста, свяжитесь с технической службой");
            return true;
        }
    }
}