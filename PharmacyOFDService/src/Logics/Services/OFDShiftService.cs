﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TS2OFDService.Helpers;
using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class OFDShiftService : IOFDShiftService
    {
        private HttpClient client;
        private JsonSerializerSettings settings;

        public OFDShiftService(string rpcApiAddress)
        {
            if (client == null)
            {
                client = new HttpClient { BaseAddress = new Uri(rpcApiAddress) };
            }
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyyMMddHHmmss",};
            settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                Converters = new List<JsonConverter>
                {
                    dateTimeConverter,
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseResponseModel OpenShift(DateTime time)
        {
            var jsonRpc = MakeRPCOpenModel(time);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<BaseResponseModel>(jsonString,settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        private JsonRpcShiftModel MakeRPCOpenModel(DateTime time)
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.OpenZReport",
                Id = 10001,
                Params = new JsonRpcShiftParams
                {
                    Time = time.ToString("yyyy-MM-dd HH:mm:ss")
                },
                Jsonrpc = "2.0"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        public BaseResponseModel CloseShift(DateTime time)
        {
            var jsonRpc = MakeRPcCloseModel(time);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<BaseResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private JsonRpcShiftModel MakeRPcCloseModel(DateTime time)
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.CloseZReport",
                Id = 10000,
                Params = new JsonRpcShiftParams
                {
                    Time = time.ToString("yyyy-MM-dd HH:mm:ss")
                },
                Jsonrpc = "2.0"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ShiftResponseModel GetCurrentShiftInfo()
        {
            var jsonRpc = MakeRPCCurrentShiftInfo();
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<ShiftResponseModel>(jsonString,settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JsonRpcShiftModel MakeRPCCurrentShiftInfo()
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.GetZReportInfo",
                Id = 30001,
                Params = new JsonRpcShiftParams
                {
                    Number = 0
                },
                Jsonrpc = "2.0"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public ShiftResponseModel GetShiftByNumber(int number)
        {
            var jsonRpc = MakeRPCShiftInfoByNumber(number);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<ShiftResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private JsonRpcShiftModel MakeRPCShiftInfoByNumber(int number)
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.GetZReportInfo",
                Id = 30001,
                Params = new JsonRpcShiftParams
                {
                    Number = number
                },
                Jsonrpc = "2.0"
            };
        }
    }
}