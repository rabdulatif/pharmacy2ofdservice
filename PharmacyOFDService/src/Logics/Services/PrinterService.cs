﻿using System;
using System.Drawing.Printing;
using System.Management;
using TS2.OFD.Service.src.Logics.Services.Interfaces;

namespace TS2.OFD.Service.src.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class PrinterService : IPrinterService
    {
        private static PrinterSettings printerSettings = new PrinterSettings();
        private static String _defaultPrinterName;
        private static String PortName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsDefaultPrinterAviable()
        {
            _defaultPrinterName = printerSettings.PrinterName;

            ManagementScope scope = new ManagementScope(@"\root\cimv2");
            scope.Connect();

            ManagementObjectSearcher searcher = new
                ManagementObjectSearcher("SELECT * FROM Win32_Printer");

            var workOffline = "true";
            foreach (ManagementObject printer in searcher.Get())
            {
                var printerName = printer["Name"].ToString().ToLower();
                if (!printerName.Equals(_defaultPrinterName.ToLower())) continue;

                PortName = printer["PortName"]?.ToString();

                if (PortName != null && PortName.Contains("USB"))
                {
                    workOffline = printer["WorkOffline"].ToString().ToLower();
                }
            }
            var isoff = !workOffline.Equals("true");
            return isoff;
        }
    }
}
