﻿using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFiscalMachineService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        FiscalMachineResponseModel GetFMStats();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool IsFmAviable();
    }
}