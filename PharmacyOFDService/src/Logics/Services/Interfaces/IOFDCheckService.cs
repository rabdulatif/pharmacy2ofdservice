﻿using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOFDCheckService
    {
        /// <summary>
        /// Получить количество неотправленных чеков
        /// </summary>
        /// <returns></returns>
        int GetUnsendCount();

        /// <summary>
        /// Отправит неотправленные чеки
        /// </summary>
        /// <returns></returns>
        BaseResponseModel ResendUnsent();

        /// <summary>
        /// Получить первый неотправленный чек
        /// </summary>
        /// <returns></returns>
        UnsendCheckResponseModel GetFirstUnsendCheckInfo();
        UnsendCheckResponseModel GetFirstUnsendCheckInfoByNumber(int number);

        /// <summary>
        /// Отправкa кассового чека
        /// </summary>
        /// <param name="check"></param>
        BaseResponseModel Send(ReceiptDTO check);

        /// <summary>
        /// Возврат кассового чека
        /// </summary>
        /// <param name="check"></param>
        BaseResponseModel Return(ReceiptDTO check);
    }
}