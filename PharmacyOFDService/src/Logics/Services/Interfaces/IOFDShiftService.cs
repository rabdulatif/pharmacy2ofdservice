﻿using System;
using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOFDShiftService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        BaseResponseModel OpenShift(DateTime time);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        BaseResponseModel CloseShift(DateTime time);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ShiftResponseModel GetCurrentShiftInfo();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ShiftResponseModel GetShiftByNumber(int number);
    }
}