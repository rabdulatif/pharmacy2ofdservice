﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using TS2OFDService.Extensions;
using TS2OFDService.Helpers;
using TS2OFDService.Models;

namespace TS2OFDService.Services
{
    public class OFDCheckService : IOFDCheckService
    {
        private HttpClient client;
        private JsonSerializerSettings settings;

        public OFDCheckService(string rpcApiAddress)
        {
            if (client == null)
            {
                client = new HttpClient { BaseAddress = new Uri(rpcApiAddress) };
            }
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyyMMddHHmmss", };
            settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                Converters = new List<JsonConverter>
                {
                    dateTimeConverter,
                }
            };
        }

        /// <summary>
        /// Api.GetUnsentCount (Yuvorilmagan cheklar sonini qaytaradi)
        /// </summary>
        /// <returns></returns>
        public int GetUnsendCount()
        {
            var jsonRpc = MakeRPCUnsendCountModel();
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<UnsendCheckResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response.Result?.Count ?? 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JsonRpcCheckModel MakeRPCUnsendCountModel()
        {
            return new JsonRpcCheckModel
            {
                Id = 3001,
                Method = "Api.GetUnsentCount",
                Params = new JsonRpcParams(),
                Jsonrpc = "2.0"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public UnsendCheckResponseModel GetFirstUnsendCheckInfoByNumber(int number)
        {
            var jsonRpc = MakeRPCUnsendCheckModel(number);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<UnsendCheckResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// Api.GetReceiptInfo (Chekni numberi boyicha birinchisini olib beradi)
        /// </summary>
        /// <returns></returns>
        public UnsendCheckResponseModel GetFirstUnsendCheckInfo()
        {
            var jsonRpc = MakeRPCUnsendCheckModel(1);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<UnsendCheckResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JsonRpcShiftModel MakeRPCUnsendCheckModel(int number)
        {
            return new JsonRpcShiftModel
            {
                Method = "Api.GetReceiptInfo",
                Id = 20001,
                Params = new JsonRpcShiftParams
                {
                    Number = number
                },
                Jsonrpc = "2.0"
            };

        }

        /// <summary>
        /// Отправкa кассового чека
        /// </summary>
        /// <returns>BaseResponseModel</returns>
        /// <param name="checkDto">Чек модель</param>
        public BaseResponseModel Send(ReceiptDTO checkDto)
        {
            var check = checkDto.ToReceipt();
            var jsonRpc = MakeRPCSendModel(check);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<BaseResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// Api.ResendUnsent(Yuvorilmagan cheklarni qayta yuvoradi)
        /// </summary>
        /// <returns></returns>
        public BaseResponseModel ResendUnsent()
        {
            var jsonRpc = MakeRPCUnsentModel();
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<BaseResponseModel>(jsonString, settings);

            response.Content = jsonString;
            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JsonRpcCheckModel MakeRPCUnsentModel()
        {
            return new JsonRpcCheckModel
            {
                Method = "Api.ResendUnsent",
                Id = 4001,
                Params = new JsonRpcParams(),
                Jsonrpc = "2.0"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="check"></param>
        private JsonRpcCheckModel MakeRPCSendModel(Receipt check)
        {
            return new JsonRpcCheckModel
            {
                Method = "Api.SendSaleReceipt",
                Id = 20001,
                Params = new JsonRpcParams
                {
                    Receipt = check
                },
                Jsonrpc = "2.0"
            };

        }

        /// <summary>
        /// Возврат кассового чека
        /// </summary>
        /// <param name="check"></param>
        public BaseResponseModel Return(ReceiptDTO checkDto)
        {
            var check = checkDto.ToReceipt();
            var jsonRpc = MakeRPCReturnModel(check);
            var postResult = client.PostAsJsonAsync("", jsonRpc).Result;

            var jsonString = postResult.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<BaseResponseModel>(jsonString, settings);
            response.Content = jsonString;

            if (response.Error != null)
                ErrorHelper.OnError(response.Error);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="check"></param>
        /// <returns></returns>
        private JsonRpcCheckModel MakeRPCReturnModel(Receipt check)
        {
            return new JsonRpcCheckModel
            {
                Method = "Api.SendRefundReceipt",
                Id = 20000,
                Params = new JsonRpcParams
                {
                    Receipt = check
                },
                Jsonrpc = "2.0"
            };
        }

    }
}