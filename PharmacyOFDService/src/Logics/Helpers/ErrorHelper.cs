﻿using System;
using System.Text;
using TS2OFDService.Models;

namespace TS2OFDService.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class ErrorHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public static void OnError(ResponseError error)
        {
            var message = new StringBuilder();
            message.AppendLine($"OFD Content: Code: {error.Code} \n Message: {error.Message} \n Data: {error.Data}");

            switch (error.Message)
            {
                case nameof(ErrorString.ERROR_RECEIPT_TIME_PAST):
                    message.AppendLine($"ERROR_RECEIPT_TIME_PAST - {ErrorString.ERROR_RECEIPT_TIME_PAST}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ACKNOWLEGE_SIGNATURE_INVALID):
                    message.AppendLine($"ERROR_ACKNOWLEGE_SIGNATURE_INVALID - {ErrorString.ERROR_ACKNOWLEGE_SIGNATURE_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ACKNOWLEGE_TERMINAL_ID_MISMATCH):
                    message.AppendLine($"ERROR_ACKNOWLEGE_TERMINAL_ID_MISMATCH - {ErrorString.ERROR_ACKNOWLEGE_TERMINAL_ID_MISMATCH}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ACKNOWLEGE_WRONG_LENGTH):
                    message.AppendLine($"ERROR_ACKNOWLEGE_WRONG_LENGTH - {ErrorString.ERROR_ACKNOWLEGE_WRONG_LENGTH}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_CLOSE_ZREPORT_TIME_PAST):
                    message.AppendLine($"ERROR_CLOSE_ZREPORT_TIME_PAST - {ErrorString.ERROR_CLOSE_ZREPORT_TIME_PAST}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_CURRENT_TIME_FORMAT_INVALID):
                    message.AppendLine($"ERROR_CURRENT_TIME_FORMAT_INVALID - {ErrorString.ERROR_CURRENT_TIME_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_CURRENT_ZREPORT_IS_EMPTY):
                    message.AppendLine($"ERROR_CURRENT_ZREPORT_IS_EMPTY - {ErrorString.ERROR_CURRENT_ZREPORT_IS_EMPTY}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_DATA_SIZE_NOT_SUPPORTED):
                    message.AppendLine($"ERROR_DATA_SIZE_NOT_SUPPORTED - {ErrorString.ERROR_DATA_SIZE_NOT_SUPPORTED}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_FIRST_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID):
                    message.AppendLine($"ERROR_FIRST_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID - {ErrorString.ERROR_FIRST_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_LAST_TRANSACTION_TIME_FORMAT_INVALID):
                    message.AppendLine($"ERROR_LAST_TRANSACTION_TIME_FORMAT_INVALID - {ErrorString.ERROR_LAST_TRANSACTION_TIME_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_LOCKED_FOREVER):
                    message.AppendLine($"ERROR_LOCKED_FOREVER - {ErrorString.ERROR_LOCKED_FOREVER}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_LOCK_CHALLENGE_INVALID):
                    message.AppendLine($"ERROR_LOCK_CHALLENGE_INVALID - {ErrorString.ERROR_LOCK_CHALLENGE_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_MAINTENANCE_REQUIRED):
                    message.AppendLine($"ERROR_MAINTENANCE_REQUIRED - {ErrorString.ERROR_MAINTENANCE_REQUIRED}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_NOT_ENOUGH_CARD_FOR_REFUND):
                    message.AppendLine($"ERROR_NOT_ENOUGH_CARD_FOR_REFUND - {ErrorString.ERROR_NOT_ENOUGH_CARD_FOR_REFUND}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_NOT_ENOUGH_CASH_FOR_REFUND):
                    message.AppendLine($"ERROR_NOT_ENOUGH_CASH_FOR_REFUND - {ErrorString.ERROR_NOT_ENOUGH_CASH_FOR_REFUND}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_COUNT_ZERO):
                    message.AppendLine($"ERROR_RECEIPT_COUNT_ZERO - {ErrorString.ERROR_RECEIPT_COUNT_ZERO}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_SPACE_IS_FULL):
                    message.AppendLine($"ERROR_ZREPORT_SPACE_IS_FULL - {ErrorString.ERROR_ZREPORT_SPACE_IS_FULL}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_PARTITION_INVALID):
                    message.AppendLine($"ERROR_ZREPORT_PARTITION_INVALID - {ErrorString.ERROR_ZREPORT_PARTITION_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_OPEN_TIME_FORMAT_INVALID):
                    message.AppendLine($"ERROR_ZREPORT_OPEN_TIME_FORMAT_INVALID - {ErrorString.ERROR_ZREPORT_OPEN_TIME_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_IS_NOT_OPEN):
                    message.AppendLine($"ERROR_ZREPORT_IS_NOT_OPEN - {ErrorString.ERROR_ZREPORT_IS_NOT_OPEN}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_IS_ALREADY_OPEN):
                    message.AppendLine($"ERROR_ZREPORT_IS_ALREADY_OPEN - {ErrorString.ERROR_ZREPORT_IS_ALREADY_OPEN}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_ZREPORT_INDEX_OUT_OF_BOUNDS):
                    message.AppendLine($"ERROR_ZREPORT_INDEX_OUT_OF_BOUNDS - {ErrorString.ERROR_ZREPORT_INDEX_OUT_OF_BOUNDS}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_SALE_REFUND_COUNT_OVERFLOW):
                    message.AppendLine($"ERROR_SALE_REFUND_COUNT_OVERFLOW - {ErrorString.ERROR_SALE_REFUND_COUNT_OVERFLOW}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID):
                    message.AppendLine($"ERROR_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID - {ErrorString.ERROR_RECEIPT_TRANSACTION_TIME_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_TOTAL_PRICE_ZERO):
                    message.AppendLine($"ERROR_RECEIPT_TOTAL_PRICE_ZERO - {ErrorString.ERROR_RECEIPT_TOTAL_PRICE_ZERO}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_TOTAL_PRICE_OVERFLOW):
                    message.AppendLine($"ERROR_RECEIPT_TOTAL_PRICE_OVERFLOW - {ErrorString.ERROR_RECEIPT_TOTAL_PRICE_OVERFLOW}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_INDEX_OUT_OF_BOUNDS):
                    message.AppendLine($"ERROR_RECEIPT_INDEX_OUT_OF_BOUNDS - {ErrorString.ERROR_RECEIPT_INDEX_OUT_OF_BOUNDS}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_NOT_FOUND):
                    message.AppendLine($"ERROR_RECEIPT_NOT_FOUND - {ErrorString.ERROR_RECEIPT_NOT_FOUND}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_FORMAT_INVALID):
                    message.AppendLine($"ERROR_RECEIPT_FORMAT_INVALID - {ErrorString.ERROR_RECEIPT_FORMAT_INVALID}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_TOTAL_PRICE_MISMATCH):
                    message.AppendLine($"ERROR_RECEIPT_TOTAL_PRICE_MISMATCH - {ErrorString.ERROR_RECEIPT_TOTAL_PRICE_MISMATCH}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_MEMORY_FULL):
                    message.AppendLine($"ERROR_RECEIPT_MEMORY_FULL - {ErrorString.ERROR_RECEIPT_MEMORY_FULL}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_RECEIPT_STORE_DAYS_LIMIT_EXCEEDED):
                    message.AppendLine($"ERROR_RECEIPT_STORE_DAYS_LIMIT_EXCEEDED - {ErrorString.ERROR_RECEIPT_STORE_DAYS_LIMIT_EXCEEDED}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_OPEN_CLOSE_ZREPORT_WRONG_LENGTH):
                    message.AppendLine($"ERROR_OPEN_CLOSE_ZREPORT_WRONG_LENGTH - {ErrorString.ERROR_OPEN_CLOSE_ZREPORT_WRONG_LENGTH}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_NOT_ENOUGH_VAT_FOR_REFUND):
                    message.AppendLine($"ERROR_NOT_ENOUGH_VAT_FOR_REFUND - {ErrorString.ERROR_NOT_ENOUGH_VAT_FOR_REFUND}");
                    throw new Exception(message.ToString());
                case nameof(ErrorString.ERROR_OPEN_ZREPORT_TIME_PAST):
                    message.AppendLine($"ERROR_OPEN_ZREPORT_TIME_PAST - {ErrorString.ERROR_OPEN_ZREPORT_TIME_PAST}");
                    throw new Exception(message.ToString());
                default:
                    throw new Exception(message.ToString());
            }
        }
    }
}