﻿using System;
using TS2OFDService.Models;
using TS2OFDService.Services;

namespace TS2OFDService.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class OFDShiftManager
    {
        /// <summary>
        ///
        /// </summary>
        private readonly IOFDShiftService service;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="rpcApiAddress">API фискального модуля</param>
        public OFDShiftManager(string rpcApiAddress)
        {
            service = new OFDShiftService(rpcApiAddress);
        }

        /// <summary>
        /// Открыть смену
        /// </summary>
        /// <param name="time">Время при открытии смены</param>
        /// <returns></returns>
        public BaseResponseModel OpenShift(DateTime time)
        {
            if (time == null)
                throw new Exception("Manager: Пожалуйста, введите дату и время, чтобы открыть смену");

            return service.OpenShift(time);
        }

        /// <summary>
        /// Закрыть смену
        /// </summary>
        /// <param name="time">Время при закрытия смены</param>
        /// <returns></returns>
        public BaseResponseModel CloseShift(DateTime time)
        {
            if(time==null)
                throw new Exception("Manager: Пожалуйста, введите дату и время, чтобы закрыть смену");

            return service.CloseShift(time);
        }

        /// <summary>
        /// Получить информацию о текущей смене
        /// </summary>
        /// <returns>ShiftResponseModel</returns>
        public ShiftResponseModel GetCurrentShiftInfo()
        {
            return service.GetCurrentShiftInfo();
        }

        /// <summary>
        /// Получить информацию о текущей смене по номеру
        /// </summary>
        /// <returns>ShiftResponseModel</returns>
        public ShiftResponseModel GetShiftInfoByNumber(int number)
        {
            return service.GetShiftByNumber(number);
        }
    }
}