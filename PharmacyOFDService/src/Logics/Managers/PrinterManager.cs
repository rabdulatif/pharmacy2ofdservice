﻿using System;
using System.Drawing.Printing;
using System.Management;
using TS2.OFD.Service.src.Logics.Services;
using TS2.OFD.Service.src.Logics.Services.Interfaces;

namespace TS2.OFD.Service.src.Logics.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class PrinterManager
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IPrinterService printerService;

        /// <summary>
        /// 
        /// </summary>
        public PrinterManager()
        {
            printerService = new PrinterService();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsDefaultPrinterAvaiable()
        {
            return printerService.IsDefaultPrinterAviable();
        }
    }

}