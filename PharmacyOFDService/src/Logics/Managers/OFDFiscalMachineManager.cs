﻿using TS2OFDService.Models;
using TS2OFDService.Services;

namespace TS2OFDService.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class OFDFiscalMachineManager
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IFiscalMachineService service;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="rpcApiAddress">API фискального модуля</param>
        public OFDFiscalMachineManager(string rpcApiAddress)
        {
            service=new FiscalMachineService(rpcApiAddress);
        }

        /// <summary>
        /// Получить статистику FM(Fiscal Machine)
        /// </summary>
        /// <returns>FiscalMachineResponseModel</returns>
        public FiscalMachineResponseModel GetFMStats()
        {
            return service.GetFMStats();
        }

        public bool IsFmAviable()
        {
            return service.IsFmAviable();
        }
    }
}