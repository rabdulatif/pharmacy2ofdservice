﻿using System;
using TS2OFDService.Models;
using TS2OFDService.Services;

namespace TS2OFDService.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class OFDCheckManager
    {
        /// <summary>
        /// Service
        /// </summary>
        private readonly IOFDCheckService service;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="rpcApiAddress">API фискального модуля</param>
        public OFDCheckManager(string rpcApiAddress)
        {
            service = new OFDCheckService(rpcApiAddress);
        }



        /// <summary>
        /// Получить информацию о первом неотправленном чеке
        /// </summary>
        /// <returns></returns>
        public UnsendCheckResponseModel GetFirstUnsendCheckInfo()
        {
            return service.GetFirstUnsendCheckInfo();
        }

        /// <summary>
        /// Получить информацию о первом неотправленном чеке
        /// </summary>
        /// <returns></returns>
        public UnsendCheckResponseModel GetFirstUnsendCheckInfoByNumber(int number)
        {
            return service.GetFirstUnsendCheckInfo();
        }

        /// <summary>
        /// Отправкa кассового чека
        /// </summary>
        /// <param name="check">Чек для отправки</param>
        public BaseResponseModel SendCheck(ReceiptDTO check)
        {
            ModelValid(check);
            return service.Send(check);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BaseResponseModel ResendUnsent()
        {
            return service.ResendUnsent();
        }

        public int GetUnsentCheckCount()
        {
            return service.GetUnsendCount();
        }

        /// <summary>
        /// Возврат кассового чека
        /// </summary>
        /// <param name="check">Чек для возврата</param>
        public BaseResponseModel ReturnCheck(ReceiptDTO check)
        {
            return service.Return(check);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="check"></param>
        private void ModelValid(ReceiptDTO check)
        {
            if (check.ReceivedCard < 0)
                throw new Exception("Свойство 'ReceivedCard(Cумма посредством пластиковой карты)' пусто");
            if (check.ReceivedCash < 0)
                throw new Exception("Свойство 'ReceivedCash(Cумма наличными)' пусто");

            check.Items.ForEach(item =>
            {
                if (item.Price < 0)
                    throw new Exception("Свойство 'Price(Cумма позиции без учета скидок)' пусто");
                if (item.Discount < 0)
                    throw new Exception("Свойство 'Discount(Cкидка)' пусто");
                if (item.Other < 0)
                    throw new Exception("Свойство 'Other(Прочие (Оплата по страховки и др.))' пусто");
                if (item.Amount < 0)
                    throw new Exception("Свойство 'Amount(Количество товара)' пусто");

                if (string.IsNullOrEmpty(item.Barcode))
                    throw new Exception("Свойство 'Barcode(Товарный код)' пусто");
                if (string.IsNullOrEmpty(item.Name))
                    throw new Exception("Свойство 'Name(Наименование товара)' пусто");

                if (item.VAT < 0)
                    throw new Exception("Свойство 'VAT(НДС сумма)' пусто");
            });
        }
    }
}