﻿
using Microsoft.AspNetCore.Mvc;
using TS2OFD.RPCService.src.Logics.Models.RPC.Connector;

namespace TS2OFD.RPCService.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [Route("[controller]/[action]")]
    public class ApiController : ControllerBase
    {
        protected const string Url = "http://127.0.0.1:3448/rpc/api";

        /// <summary>
        /// 
        /// </summary>
        protected readonly IRpcConnector _rpcConnector;

        /// <summary>
        /// 
        /// </summary>
        public ApiController()
        {
            _rpcConnector = new RpcConnector();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <returns>string Result</returns>
        [HttpPost]
        public ActionResult SendRequest(string method,object[] obj)
        {
            var result = _rpcConnector.MakeRequest<string>(Url,method, obj);
            return Ok(result);
        }
    }
}
