﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TS2OFD.RPCService.src.Logics.ExceptionHandling
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class RpcException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public RpcException() { }

        public RpcException(string customMessage) : base(customMessage)
        {
        }

        public RpcException(string customMessage, Exception exception) : base(customMessage, exception)
        {
        }
    }
}
