﻿using TS2OFD.RPCService.src.Logics.Models.RPC.Specification;

namespace TS2OFD.RPCService.src.Logics.Models.RPC.Connector
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRpcConnector
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        T MakeRequest<T>(string url,string method, object[] obj);
    }
}
