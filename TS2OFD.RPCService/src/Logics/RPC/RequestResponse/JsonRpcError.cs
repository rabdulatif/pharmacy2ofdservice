﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TS2OFD.RPCService.src.Logics.Models.RPC.RequestResponse
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonRpcError
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}
