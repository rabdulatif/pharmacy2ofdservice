﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TS2OFD.RPCService.src.Logics.Models.RPC.RequestResponse
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonRpcRequest
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        public JsonRpcRequest(int id, string method, object[] obj)
        {
            Id = id;
            Method = method;
            Parameters = obj;
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "method", Order = 0)]
        public string Method { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "id", Order = 1)]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "params", Order = 2)]
        public IList<object> Parameters { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public byte[] GetBytes()
        {
            var json = JsonConvert.SerializeObject(this);
            return Encoding.UTF8.GetBytes(json);
        }
    }
}
