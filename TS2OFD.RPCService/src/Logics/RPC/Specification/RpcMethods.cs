﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TS2OFD.RPCService.src.Logics.Models.RPC.Specification
{
    /// <summary>
    /// 
    /// </summary>
    public class RpcMethods
    {
        /// <summary>
        /// Send check
        /// </summary>
        public const string SendSaleReceipt = "Api.SendSaleReceipt";
        /// <summary>
        /// Return check
        /// </summary>
        public const string SendRefundReceipt = "Api.SendRefundReceipt";
        /// <summary>
        /// Open shift
        /// </summary>
        public const string OpenZReport = "Api.OpenZReport";
        /// <summary>
        /// Close shift
        /// </summary>
        public const string CloseZReport = "Api.CloseZReport";
    }
}
