﻿using System;

namespace TS2OFD.Service.Models.Exceptions
{
    public class VersionNotValidException : Exception
    {
        public VersionNotValidException(string message): base(message)
        {
        }
    }
}
