﻿namespace TS2OFD.Service.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum PrinterState
    {
        /// <summary>
        /// Доступен
        /// </summary>
        Aviable,
        /// <summary>
        /// Недоступен
        /// </summary>
        NotAviable
    }
}
