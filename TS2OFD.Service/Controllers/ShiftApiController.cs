﻿using Microsoft.AspNetCore.Mvc;
using System;
using TS2OFDService.Managers;
using TS2OFDService.Models;

namespace TS2OFD.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class ShiftApiController : ControllerBase
    {
        private OFDShiftManager _shiftManager;

        /// <summary>
        /// 
        /// </summary>
        public ShiftApiController()
        {
            _shiftManager = new OFDShiftManager(Program.FMApiAddress);
        }

        /// <summary>
        /// Tekushaya smena info
        /// </summary>
        /// <returns>ShiftResponseModel</returns>
        [HttpGet]
        [Produces(typeof(ShiftResponseModel))]
        public ActionResult GetCurrentShift()
        {
            try
            {
                var shiftInfo = _shiftManager.GetCurrentShiftInfo();
                return Ok(shiftInfo);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns>ShiftResponseModel</returns>
        [HttpGet]
        [Produces(typeof(ShiftResponseModel))]
        public ActionResult GetCurrentShiftByNumber(int number)
        {
            try
            {
                var shiftInfo = _shiftManager.GetShiftInfoByNumber(number);
                return Ok(shiftInfo);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>BaseResponseModel</returns>
        [HttpPost]
        [Produces(typeof(BaseResponseModel))]
        public ActionResult OpenShift(DateTime dateTime)
        {
            try
            {
                var res = _shiftManager.OpenShift(dateTime);
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>BaseResponseModel</returns>
        [HttpPost]
        [Produces(typeof(BaseResponseModel))]
        public ActionResult CloseShift(DateTime dateTime)
        {
            try
            {
                var res = _shiftManager.CloseShift(dateTime);
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }
    }
}
