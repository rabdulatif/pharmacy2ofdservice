﻿using Microsoft.AspNetCore.Mvc;
using System;
using TS2OFDService.Managers;
using TS2OFDService.Models;

namespace TS2OFD.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class FMApiController : ControllerBase
    {
        private OFDFiscalMachineManager _fmManager;
        
        /// <summary>
        /// 
        /// </summary>
        public FMApiController()
        {
            _fmManager = new OFDFiscalMachineManager(Program.FMApiAddress);
        }

        /// <summary>
        /// Получить статистику FM(Fiscal Machine)
        /// </summary>
        /// <returns>FiscalMachineResponseModel</returns>
        [HttpGet]
        [Produces(typeof(FiscalMachineResponseModel))]
        public ActionResult GetInfo()
        {
            try
            {
                var stat = _fmManager.GetFMStats();
                return Ok(stat);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Boolean</returns>
        [HttpGet]
        [Produces(typeof(Boolean))]
        public ActionResult GetIsModuleAviable()
        {
            try
            {
                var res = _fmManager.IsFmAviable();
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

    }
}
