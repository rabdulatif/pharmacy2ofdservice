﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TS2.OFD.Service.src.Logics.Managers;
using TS2OFD.Service.Models;

namespace TS2OFD.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class PrinterController : ControllerBase
    {
        private PrinterManager _printerManager;

        /// <summary>
        /// 
        /// </summary>
        public PrinterController()
        {
            _printerManager = new PrinterManager();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces(typeof(string))]
        public ActionResult GetPrinterState()
        {
            try
            {
                var isPrinterAviable = _printerManager.IsDefaultPrinterAvaiable();
                if (isPrinterAviable)
                    return Ok(PrinterState.Aviable.ToString());
                else
                    return Ok(PrinterState.NotAviable.ToString());
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(PrinterState.NotAviable);
            }
        }
    }
}
