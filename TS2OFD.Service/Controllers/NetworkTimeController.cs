﻿using System;
using Microsoft.AspNetCore.Mvc;
using TS2.OFD.Service.src.Logics.Managers;

namespace TS2OFD.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class NetworkTimeController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces(typeof(DateTime))]
        public ActionResult GetNetworkTime()
        {
            var networkTime = InternetTimeManager.GetCurrentInternetTime();
            return Ok(networkTime?.ToString("DD.MM.YYYY HH:mm:ss"));
        }
    }
}
