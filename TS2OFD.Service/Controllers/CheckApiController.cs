﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TS2OFDService.Managers;
using TS2OFDService.Models;

namespace TS2OFD.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class CheckApiController : ControllerBase
    {
        private OFDCheckManager _checkManager;

        /// <summary>
        /// 
        /// </summary>
        public CheckApiController()
        {
            _checkManager = new OFDCheckManager(Program.FMApiAddress);
        }

        /// <summary>
        /// Получить информацию о первом неотправленном чеке
        /// </summary>
        /// <returns>UnsendCheckResponseModel</returns>
        [HttpGet]
        [Produces(typeof(UnsendCheckResponseModel))]
        public ActionResult GetUnsendCheck()
        {
            try
            {
                var unsendCheck = _checkManager.GetFirstUnsendCheckInfo();
                return Ok(unsendCheck);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о первом неотправленном чеке
        /// </summary>
        /// <param name="number"></param>
        /// <returns>UnsendCheckResponseModel</returns>
        [HttpGet]
        [Produces(typeof(UnsendCheckResponseModel))]
        public ActionResult GetUnsendCheckByNumber(int number)
        {
            try
            {
                var unsendCheck = _checkManager.GetFirstUnsendCheckInfoByNumber(number);
                return Ok(unsendCheck);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// Otilmagan checklani sonini chiqazadi
        /// </summary>
        /// <returns>int</returns>
        [HttpGet]
        [Produces(typeof(int))]
        public ActionResult GetUnsendCheckCount()
        {
            try
            {
                var unsendCount = _checkManager.GetUnsentCheckCount();
                return Ok(unsendCount);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// Отправкa кассового чека
        /// </summary>
        /// <param name="check"></param>
        /// <returns>Result of sending check to OFD</returns>
        [HttpPost]
        [Produces(typeof(BaseResponseModel))]
        public ActionResult SendCheck(ReceiptDTO check)
        {
            try
            {
                var res = _checkManager.SendCheck(check);
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// Переотправкa кассового чека
        /// </summary>
        /// <returns>Result of sending check to OFD</returns>
        [HttpGet]
        [Produces(typeof(BaseResponseModel))]
        public ActionResult ResendUnsendChecks()
        {
            try
            {
                var res = _checkManager.ResendUnsent();
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }

        /// <summary>
        /// Возврат кассового чека
        /// </summary>
        /// <param name="check"></param>
        /// <returns>Result of sending check to OFD</returns>
        [HttpPost]
        [Produces(typeof(BaseResponseModel))]
        public ActionResult ReturnCheck(ReceiptDTO check)
        {
            try
            {
                var res = _checkManager.ReturnCheck(check);
                return Ok(res);
            }
            catch (Exception ex)
            {
                //TODO: Add Log information, write to Log.txt
                return Ok(ex.Message);
            }
        }
    }
}
