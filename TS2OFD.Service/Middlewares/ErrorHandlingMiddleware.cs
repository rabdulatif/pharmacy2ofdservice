﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TS2OFD.Service.Models.Exceptions;

namespace TS2OFD.Service.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                context.Request.EnableBuffering();
                await HandleExceptionAsync(context, logger, ex);
            }
        }

        private static async Task<Task> HandleExceptionAsync(HttpContext context, ILogger logger, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected

            if (exception is MyNotFoundException) code = HttpStatusCode.NotFound;
            else if (exception is MyUnauthorizedException) code = HttpStatusCode.Unauthorized;

            if (!String.IsNullOrWhiteSpace(context.User?.Identity?.Name))
            {
                //First, get the incoming request
                var request = await FormatRequest(context.Request);

                var filename = $"Logs\\{context.User?.Identity?.Name}_server.txt";
                File.AppendAllText(filename, $"{Environment.NewLine}{Environment.NewLine}USER:{context.User?.Identity?.Name} {DateTime.Now}{Environment.NewLine}{request}{Environment.NewLine}{exception.Message}");
            }
            else if (!(exception is VersionNotValidException))
            {
                //First, get the incoming request
                var request = await FormatRequest(context.Request);

                logger.LogError($"{Environment.NewLine}{Environment.NewLine}USER:{context.User?.Identity?.Name}{Environment.NewLine}{request}{Environment.NewLine}{exception.Message} ");
            }

            var result = JsonConvert.SerializeObject(new { error = exception.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (Int32)code;
            return context.Response.WriteAsync(result);
        }

        private static async Task<string> FormatRequest(HttpRequest request)
        {
            var bodyAsText = "";
            request.EnableBuffering();
            using (var reader = new StreamReader(request.Body, Encoding.UTF8))
            {
                request.Body.Seek(0, SeekOrigin.Begin);
                bodyAsText = reader.ReadToEnd();
                request.Body.Seek(0, SeekOrigin.Begin);
            }

            return $"ApiRequest {request.Path} {request.Method}  {Environment.NewLine} {bodyAsText}";
        }
    }
}
